package ru.tsc.panteleev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataBinaryLoadRequest;
import ru.tsc.panteleev.tm.event.ConsoleEvent;

@Component
public class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file";

    @NotNull
    private static final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinaryLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showDescription();
        getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

}
