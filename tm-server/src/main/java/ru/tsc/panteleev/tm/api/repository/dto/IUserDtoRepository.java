package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    UserDto findByLogin(@NotNull String login);

    UserDto findByEmail(@NotNull String email);

}
