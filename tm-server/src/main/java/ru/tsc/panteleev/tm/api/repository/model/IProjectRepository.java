package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.panteleev.tm.model.Project;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sortColumn")
    List<Project> findAllByUserIdSort(@Nullable @Param("userId") String userId,
                                      @Nullable @Param("sortColumn") String sortColumn);

    @Nullable
    Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}
