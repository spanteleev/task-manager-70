package ru.tsc.panteleev.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.panteleev.tm.api.service.IDatabaseProperty;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;
import ru.tsc.panteleev.tm.model.AbstractModel;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.panteleev.tm")
@EnableJpaRepositories("ru.tsc.panteleev.tm.api.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUserName());
        dataSource.setPassword(databaseProperty.getDatabaseUserPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModelDto.class.getPackage().getName(),
                AbstractModel.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondLvlCache());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseCacheConfigFile());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseCacheFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseCacheRegionPrefix());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
