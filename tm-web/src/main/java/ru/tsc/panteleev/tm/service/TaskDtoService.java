package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.panteleev.tm.api.service.dto.ITaskDtoService;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @Modifying
    @Transactional
    public void add(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    @Modifying
    @Transactional
    public TaskDto create(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @Nullable final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName("New Task " + System.currentTimeMillis() / 1000);
        task.setDescription(UUID.randomUUID().toString());
        taskRepository.saveAndFlush(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public List<TaskDto> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return taskRepository.countByUserId(userId);
    }

    @Override
    @Modifying
    @Transactional
    public void save(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
    }

    @Override
    @Modifying
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Modifying
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final TaskDto task) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        deleteById(userId, task.getId());
    }

    @Override
    @Modifying
    @Transactional
    public void deleteAll(@Nullable final String userId, @Nullable final List<TaskDto> taskList) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(taskList).orElseThrow(TaskNotFoundException::new);
        taskList.forEach(task -> deleteById(userId, task.getId()));
    }

    @Override
    @Modifying
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        taskRepository.deleteByUserId(userId);
    }

}